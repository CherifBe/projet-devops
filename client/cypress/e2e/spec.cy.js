describe('My First Test', () => {
  it('Does not do much!', () => {
 
    cy.intercept(
      {
        method:'POST',
        hostname: 'localhost',
      }, 
      {
        statusCode: 200,
        body: {
          "data":
            {
              "getUser":[
              {"id":1,"lastname":"Clément","firstname":"Hugo","email":"Hugo-clément@ecole-ipssi.net","__typename":"UserInventory"},{"id":2,"lastname":"Jonas","firstname":"Blaise","email":"Blaise.Jonas@ecole-ipssi.net","__typename":"UserInventory"},{"id":3,"lastname":"Silva","firstname":"Lionel","email":"Lionel.silva@ecole-ipssi.net","__typename":"UserInventory"},{"id":4,"lastname":"BELLAHOUEL","firstname":"Cherif","email":"cherif.b@gmail.com","__typename":"UserInventory"},{"id":5,"lastname":"OLAOYE","firstname":"Nicolas","email":"nicolas@gmail.com","__typename":"UserInventory"},{"id":6,"lastname":"SILI","firstname":"Nassim","email":"nassim@gmail.com","__typename":"UserInventory"},{"id":7,"lastname":"CHAMASSE","firstname":"Abouscharaf Aboubaca","email":"abouscharaf@gmail.com","__typename":"UserInventory"},{"id":16,"lastname":"DUPONT","firstname":"Jean","email":"jean.dupont@gmail.com","__typename":"UserInventory"},{"id":17,"lastname":"DUPONT","firstname":"Jean","email":"jean.dupont@gmail.com","__typename":"UserInventory"},{"id":18,"lastname":"DUPONT","firstname":"Jean","email":"jean.dupont@gmail.com","__typename":"UserInventory"},{"id":19,"lastname":"LUNAR","firstname":"Josh","email":"josh.lunar@gmail.com","__typename":"UserInventory"}]
            }
          
        }
      }
    ).as("DATA")

    cy.visit('http://localhost:8085/');

    cy.wait("@DATA");

    cy.get('#firstname').type('John');
    cy.get('#lastname').type('DOE');
    cy.get('#email').type('john.doe@gmail.com');
    cy.get('#password').type('123456');
    cy.get('#phonenumber').type('0610101010');
  });
});