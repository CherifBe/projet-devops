import React, { Component } from 'react';
import { useQuery, useMutation, gql } from '@apollo/client';
import './App.css'

const INSERT_USER = gql`
mutation createUser($userData: UserInput!){
    createUser(userData: $userData){
      user{
        id
        firstname
        lastname
        email
        password
        phoneNumber
      }
    }
  }
`;


const GET_USERS = gql`
{
  getUser{
    id
    lastname
    firstname
    email
  }
}
  `;

function DisplayUsers(){
  const { loading, error, data } = useQuery(GET_USERS, {
    fetchPolicy: "no-cache" 
  });
  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error : {error.message}</p>;
  console.log(data.getUser);
  return data.getUser.map(({ id, lastname, firstname, email }) => (
    <li key={ id }>{ firstname } - { lastname } - { email }</li>
  ));

}

function AddUser(){
  let inputFirstName;
  let inputLastName;
  let inputEmail;
  let inputPassword;
  let inputPhoneNumber;

  const [addUser, { loading, error }] = useMutation(INSERT_USER);

  if (loading) return 'Submitting...';
  if (error) return `Submission error! ${error.message}`;

  return (
    <div className='App-form'>
      <form
        onSubmit={e=> {
          e.preventDefault();
          addUser({ variables: { userData: { firstname: inputFirstName.value, lastname: inputLastName.value, email: inputEmail.value, password: inputPassword.value, phoneNumber: inputPhoneNumber.value }} });
          inputFirstName.value = '';
          inputLastName.value = '';
          inputEmail.value = '';
          inputPassword.value = '';
          inputPhoneNumber.value = '';

        }}
      >
        <div>
          <label>Firstname</label>
          <input ref={node => { inputFirstName = node; }} id="firstname" />
        </div>

        <div>
          <label>Lastname
            <input ref={node => { inputLastName = node; }} id="lastname" />
          </label>
        </div>

        <div>
          <label>Email
            <input ref={node => { inputEmail = node; }} id="email" />
          </label>
        </div>

        <div>
          <label>Password
            <input ref={node => { inputPassword = node; }} id="password" />
          </label>
        </div>
        
        <div>
          <label>Phone number
            <input ref={node => { inputPhoneNumber = node; }} id="phonenumber" />
          </label>
        </div>

        <div>
          <button type="submit" id="btn-submit">Add User</button>
        </div>

      </form>  
    </div>
  );
}

class App extends Component {

  render() {
    return (
      <div id="main">
        <section>
          <DisplayUsers />
        </section>
  
        <section>
          <AddUser />
        </section>
      </div>
    );
  }
}

export default App;
