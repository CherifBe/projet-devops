from fastapi.testclient import TestClient
import requests
import json

def test_get_main():
    query_list_users = """
    {
        getUser{
            id
            lastname
            firstname
        }
    }
    """
    response = requests.get('http://localhost:8000/', json={'query': query_list_users})
    assert response.status_code == 200

def test_post_main():
    query_insert_user = """
        mutation{
            createUser(userData: {firstname: "John", lastname: "DOE", email: "john.doe@gmail.com", password: "123456", phoneNumber: "0610101010"}) {
                user{
                firstname
                lastname
                email
                password
                phoneNumber
                }
            }
        }
    """
    response = requests.post('http://localhost:8000/', json={'query': query_insert_user})
    awaited_response = """
    {
        "data": {
            "createUser": {
                "user": {
                    "firstname": "John",
                    "lastname": "DOE",
                    "email": "john.doe@gmail.com",
                    "password": "123456",
                    "phoneNumber": "0610101010"
                }
            }
        }
    }
    """
    assert response.status_code == 200
    assert response.json() == json.loads(awaited_response)