import graphene
from .query import Query
from .type import UserInventory, BookInventory, CreateUser
from fastapi import FastAPI
from starlette_graphene3 import GraphQLApp, make_graphiql_handler
from graphene import List, ObjectType


class MyQuery(Query):
    get_user = List(UserInventory)
    get_book = List(BookInventory)

class MyMutations(ObjectType):
    create_user = CreateUser.Field()

schema = graphene.Schema(query=MyQuery, mutation=MyMutations)

app = FastAPI()

from fastapi.middleware.cors import CORSMiddleware

origins = [
    "http://localhost:*",
    "http://localhost:8085",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.mount("/", GraphQLApp(schema, on_get=make_graphiql_handler()))

