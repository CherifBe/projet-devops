from graphene import ObjectType, String, Int, Mutation, Boolean, Field, InputObjectType
from .db import session, User

class UserInventory(ObjectType):
    id = Int()
    lastname = String()
    firstname = String()
    email = String()
    password = String()
    phone_number = String()

class BookInventory(ObjectType):
    id = Int()
    title = String()
    author = String()
    publication_date = String()

class UserInput(InputObjectType):
    lastname = String(required=True)
    firstname = String(required=True)
    email = String(required=True)
    password = String(required=True)
    phone_number = String(required=True)


class CreateUser(Mutation):
    class Arguments:
        user_data = UserInput(required=True)
    
    user = Field(UserInventory)

    def mutate(root, info, user_data=None):
        user = User(
            lastname=user_data.lastname, 
            firstname=user_data.firstname, 
            email=user_data.email, 
            password=user_data.password, 
            phone_number=user_data.phone_number
            )
        
        session.add(user)
        session.commit()

        return CreateUser(user=user)
    

