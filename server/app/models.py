from .db import engine,meta
from sqlalchemy import Table, Column, Integer, String
from sqlalchemy.sql.sqltypes import Integer, String, Date, Float, Numeric, BigInteger

user = Table('user', meta,
    Column('id', Integer, primary_key=True),
    Column('lastname', String(128)),
    Column('firstname', String(128)),
    Column('email', String(128)),
    Column('password', String(255)),
    Column('phone_number', String(128)),
)

book = Table('book', meta,
    Column('id', Integer, primary_key=True),
    Column('title', String(128)),
    Column('author', String(128)),
    Column('publication_date',Date)
)

meta.create_all(engine)