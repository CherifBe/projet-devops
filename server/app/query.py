from graphene import ObjectType
from .models import user, book
from .db import conn

class Query(ObjectType):
    def resolve_get_user(root, info):
        return conn.execute(user.select().limit(20)).fetchall()
    def resolve_get_book(root, info):
        return conn.execute(book.select().limit(20)).fetchall()
    