from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (
    create_engine, 
    MetaData,
    String,
    Integer,
    Column,
    )
import os
from sqlalchemy.orm import sessionmaker, scoped_session

host = os.environ.get("MYSQL_HOST")
port = os.environ.get("MYSQL_PORT")
username = os.environ.get("MYSQL_USERNAME")
password = os.environ.get("MYSQL_PASSWORD")
database = os.environ.get("MYSQL_DATABASE")

Base=declarative_base()

URL="mysql+pymysql://" + username + ":" + password + "@" + host + ":" + port + "/" + database

print("URL: " + URL)

engine = create_engine(URL, echo=True)

meta = MetaData()
session = scoped_session(sessionmaker(bind=engine))
conn = engine.connect()
Base.query = session.query_property()


class User(Base):
    __tablename__="user"
    id=Column(Integer(),primary_key=True)
    lastname=Column(String(128),nullable=False)
    firstname=Column(String(128),nullable=False)
    email=Column(String(128), nullable=False)
    password=Column(String(255),nullable=False)
    phone_number=Column(String(128),nullable=False)
