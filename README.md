## To install the project :  

#### If you are on mac or linux, type the following two commands :  

* python3 -m venv venv  
* . venv/bin/activate 

#### Or, if you are on windows :  

* py -3 -m venv venv  
* venv\Scripts\activate  


## So now, let's install all the packages :  

* pip install -r requirements.txt  

## Okay, now we need to start docker :  
*(Start docker desktop on your side and type the following command)*  
#### If you are on mac/linux:  

* sudo docker-compose up -d  

#### If you are on windows:  
*(the same without the "sudo")*  

* docker-compose up -d  


